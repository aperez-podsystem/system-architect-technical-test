var express = require('express');
var app = express();

var MongoClient = require('mongodb').MongoClient;

// Connection URL
var url = 'mongodb://localhost:27017/technicaltest';


MongoClient.connect(url, function(err, db) {
  if(err) throw err;

  app.locals.pretty = true;

  app.get('/', function (req, res) {
    res.send('Hello World')
  });

  app.get('/simcard', function(req, res) {
    var simcard = db.collection('simcard');
    var page = req.query.page || 0;
    simcard.find({}, {limit: 10, skip: page}).toArray(function(err, docs) {
      res.json(docs);
    })
  });

  app.get('/cdr', function(req, res) {
    var cdr = db.collection('cdr');
    var page = req.query.page || 0;
    var customer = req.query.customer;
    if(!customer) return res.json({error: 'customer not found'})
    var query = {customer: customer};
    if(req.query.iccid) query.iccid = req.query.iccid;
    if(req.query.creditLimit) query.creditLimit = req.query.creditLimit;
    if(req.query.msisdn) query.msisdn = req.query.msisdn;

    console.log(query)
    cdr.find(query, {limit: 10, skip: page}).toArray(function(err, docs) {
      res.json(docs);
    })
  });
  app.listen(3000);
});
